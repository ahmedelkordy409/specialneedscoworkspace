import React from 'react';
import axios from 'axios';


const reducer = (state, action) => {
  switch (action) {
    case 'OPEN':
      return true
    case 'CLOSE':
      return false
    default:
      throw new Error()
  }
}


export default function useModal() {
  // dialog togled using reducer  
  const [state, dispatch] = React.useReducer(reducer, false);
  const handleClickOpen = () => dispatch('OPEN');
  const handleClose = () => dispatch('CLOSE');
    
    
  return state;
}
