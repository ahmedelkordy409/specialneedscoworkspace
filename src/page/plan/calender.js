import React, {useState, useEffect } from 'react';
import CalCol from './../../component/tasks/calcol';

import SidePlanNav from './../../component/planside';

import axios from 'axios';

import { useParams} from "react-router";
import './../../stylefile/cornjob.css';
import {  Link ,useHistory } from "react-router-dom";



function Calender() {
    
// let history = useHistory();
// const { id , wid} = useParams();
 const [loading, setLoading] = useState(true);
//const [errorMessage, setErrorMessage] = useState(null);  
 const [data, setData] = useState({ calender: [], nextweek: null , bervous:  null,}); 
 
 const [week, setWeek] = useState(5); 

    
    let wnum = 5;
    
   useEffect(() => {
    const fetchData = async () => {
      const result = await axios({
          method: 'get',
          url: `https://apis.figruop.com/myapissn/public/api/calender/${week}`,
          headers: {'X-Requested-With': 'XMLHttpRequest'},
          responseType: 'json',
          status: 200,
        }

      );
      setData(result.data);
        
      setLoading(false);
        
        
    };
      
    fetchData();
  }, []);   
    

    
   
    
    
    
    return (
      <div>
        
        

        
        
        
<div style={{position: 'absolute',left: '0',top: '0',width: '300px',height: '100%',boxShadow: '1px 1px 1px #bebcbf4f',backgroundColor: 'white',}}>
        
      <SidePlanNav />  
        
</div>       
        
        
        
        
        
        
        
        
        
    
        
<main className="cornjob__content  scroll" style={{background: 'rgba(179, 179, 179, 0.02)',
        height: '100vh' ,padding: '50px', marginLeft: '300px',paddingBottom: '100px',}}> 


        

<div className="columns cal__header">
        <div className="column week__name">
         <i className="fas fa-calendar-alt cicon"></i> 
           الاسبوع الاول
        </div>
        <div className="column task__content">
           <div className="task__method">
           <a  className="week_nav"> الاسبوع السابق </a>
           </div>
           <div  className="week_date">
             <span className="date__after">   من </span>
                ١ رجب , ١٤١٦     
          </div>         
        </div>         
        <div className="column task__content">
           <div className="task__method">
             <a  className="week_nav" onClick={()=>setWeek(week+1)}> الاسبوع التالى </a>
           </div>
           <div  className="week_date">
                -
              <span  className="date__after">   الى </span>
                ١ شعبان , ١٤١٦  
           </div>  
        </div>               

</div>        
        


 
 
 





        
        
        <div className="columns is-multiline"> 
            {data.calender.map(item => (
                <CalCol 
                dayId={item.id}
                planId="2"
                dd={item.dd}
                mm={item.mm}
                name={item.day_name_id}
                />
             ))} 
        </div>

        
   </main>     
        
        
        
        
        

        
        
        
        
        
        </div>
    );
  }
  
export default Calender;