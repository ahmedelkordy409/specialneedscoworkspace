import React from 'react';
import { useParams} from "react-router";
import GoalsList from './../../component/goalslist';
import AddGoal from './../../component/addgoal';




function PlanGoals() {
  let { id } = useParams();


    return (
      <div>

       <GoalsList planId={id} />

       <AddGoal planId={id} />
        
        
        
      </div>
    );
  }
  



export default PlanGoals;
