import React from 'react';

import {makeStyles,} from '@material-ui/core/styles';

import Button from '@material-ui/core/Button';

import TextField from '@material-ui/core/TextField';

import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';

import HijriUtils from "@date-io/hijri";


import "moment/locale/ar-sa";



import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
    
  TimePicker,
  DateTimePicker,
  DatePicker,
    
    
} from '@material-ui/pickers';



const style = {
  background: 'rgb(236, 145, 121)',
  borderRadius: 0,
  border: 0,
  color: 'white',
  height: 48,
  padding: '0 30px',
  boxShadow: 'rgba(255, 255, 255, 0.3) 0px 3px 5px 2px',
};


const useStyles = makeStyles(theme => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
        
    }

  
  },
}));



function PlanCreate() {

const classes = useStyles();
const [selectedDate, setSelectedDate] = React.useState(new Date('2014-08-18T21:11:54'));

  const handleDateChange = date => {
    setSelectedDate(date);
  };
    
    
    
    return (
        
        
<div>
        
<h1> خطة جديدة </h1>
 <div className={classes.root}>
      <Button variant="contained" style={style}>
        انشاء
      </Button>
    </div>     
        
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Grid container justify="space-around">
        <KeyboardDatePicker
          disableToolbar
          variant="inline"
          format="MM/dd/yyyy"
          margin="normal"
          id="date-picker-inline"
          label="Date picker inline"
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
        <KeyboardDatePicker
          margin="normal"
          id="date-picker-dialog"
          label="Date picker dialog"
          format="MM/dd/yyyy"
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
        <KeyboardTimePicker
          margin="normal"
          id="time-picker"
          label="Time picker"
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{
            'aria-label': 'change time',
          }}
        />
        
        
        
        
        
        
      </Grid>
        

   
        
    </MuiPickersUtilsProvider> 
        
        
       
        <TextField
          className={classes.margin}
          label="ThemeProvider"
          id="mui-theme-provider-standard-input"
        />
        <TextField
          className={classes.margin}
          label="ThemeProvider"
          variant="outlined"
          id="mui-theme-provider-outlined-input"
        />
        
       
        
            
  
        
           <MuiPickersUtilsProvider utils={HijriUtils} locale="ar-SA">
        <KeyboardDatePicker
        clearable
        okLabel="موافق"
        cancelLabel="الغاء"
        clearLabel="مسح"
        label="Date picker inline"
        value={selectedDate}
        onChange={handleDateChange}
        minDate="1937-03-14"
        maxDate="2076-11-26"
        labelFunc={date => (date ? date.format("iYYYY/iMM/iDD") : "")}
        KeyboardButtonProps={{
            'aria-label': 'change date',
        }}
      />

      <TimePicker
        clearable
        okLabel="موافق"
        cancelLabel="الغاء"
        clearLabel="مسح"
        labelFunc={date => (date ? date.format("hh:mm A") : "")}
        value={selectedDate}
        onChange={handleDateChange}
      />

      <DateTimePicker
        okLabel="موافق"
        cancelLabel="الغاء"
        labelFunc={date => (date ? date.format("iYYYY/iMM/iDD hh:mm A") : "")}
        value={selectedDate}
        onChange={handleDateChange}
        minDate="1937-03-14"
        maxDate="2076-11-26"
      />
    </MuiPickersUtilsProvider>
        
        
        
        
        
</div>
        
        
   
        
        
        
    );
  }
  
export default PlanCreate;