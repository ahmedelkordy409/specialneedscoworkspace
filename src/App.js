import React from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";

import Header from './component/header';
import SideNav from './component/sidenav';
import Modal from './component/modal';



// pages
import SingePost from './page/single';
import Home from './page/home';


// plan page
import PlanIndex from './page/plan/index';
import PlanCreate from './page/plan/create';
import PlanGoals from './page/plan/goals';
import Calender from './page/plan/calender';


// auth system
import LoginPage from './secureAuth/login';




/*

const initialState = {
  isAuthenticated: false,
  user: null,
  token: null,
};
const reducer = (state, action) => {
  switch (action.type) {
    case "LOGIN":
      localStorage.setItem("user", JSON.stringify(action.payload.user));
      localStorage.setItem("token", JSON.stringify(action.payload.token));
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.user,
        token: action.payload.token
      };
    case "LOGOUT":
      localStorage.clear();
      return {
        ...state,
        isAuthenticated: false,
        user: null
      };
    default:
      return state;
  }
};*/



function App() {

 /* const [state, dispatch] = React.useReducer(reducer, initialState);
 
     <AuthContext.Provider
      value={{
        state,
        dispatch
      }}
    >
      
 
 */

  return ( 
         
  
      
      
      
    <div>


      
   
      
 <div  style={{marginRight: '75px'}}>
                <Router>
      
      

   <Header />
   
  
   <SideNav />
    
      

      
    <div className="pages" style={{position: 'relative',
    height: '100% !important',
    overflow: 'auto',
    height: '100vh',
    backgroundColor: 'rgba(162, 174, 209, 0.01)',  
      }}>
      
      
      
   
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      
      


      
      
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/post/" component={SingePost} />
      
                    <Route path="/plan/all" component={PlanIndex} />
                    <Route path="/plan/create" component={PlanCreate} />
                    <Route path="/plan/goals/:id" component={PlanGoals} />
                    <Route path="/plan/:id/week/:wid" component={Calender} />

                    <Route exact path="/login" component={LoginPage} />
                </Switch>
      
      
      
      
      
      
      
      
      
      
      
      
      
      
    </div>   
      
  
       </Router>

      
      
      
      
      
      
      
      
      
      
 


      
      

      
 </div>      
      
      
      
      
      
      
      
      
      
      
      
     
     
      
      
              


      
      
      
      
      
      
      
      
      


    </div>





  );
}

export default App;
