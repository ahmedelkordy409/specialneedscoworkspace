import React from 'react';
import ReactDOM from 'react-dom';
//import './index.css';
import './stylefile/bulma.css';
import './stylefile/all.min.css';
import './stylefile/bulma.css';
import './stylefile/reset.css';
import './stylefile/normalize.css';


import { createMuiTheme } from "@material-ui/core/styles";
import { StylesProvider, ThemeProvider } from "@material-ui/styles";
import { create } from "jss";
import rtl from "jss-rtl";
import { jssPreset } from "@material-ui/core/styles";
import { green } from '@material-ui/core/colors';





import App from './App';
import * as serviceWorker from './serviceWorker';



const jss = create({ plugins: [...jssPreset().plugins, rtl()] });
const theme = createMuiTheme({
  typography: { useNextVariants: true },
  direction: "rtl",
  fontFamily: "'Tajawal', sans-serif !important",
  palette: {
    primary: { main: '#4baaf5' },
      
  },  
    
});






ReactDOM.render(
    <StylesProvider jss={jss}>
    <ThemeProvider theme={theme}>
    <App />
    </ThemeProvider>
    </StylesProvider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
