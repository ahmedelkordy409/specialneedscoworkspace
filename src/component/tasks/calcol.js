import React, {useState, useEffect } from 'react';
import Task from './task';
import axios from 'axios';
import Create from './create';








const Days = [
    'لايوم',
    'السبت',
    'الاحد',
    'الاثنين',
    'الثلاثاء',
    'الاربعاء',
    'الخميس',
    'الجمعة',
];


const Month = [
'لا شهر', 
'يناير', 
'فبراير', 
'مارس', 
'إبريل', 
'مايو', 
'يونيو', 
 'يوليو', 
 'أغسطس', 
'سبتمبر', 
 'أكتوبر',
'نوفمبر',
'ديسمبر',
];





function CalCol(props) {

      //  
      //  
  
/***********************************************************
************************************************************
** prams @palnID , @dayId                             ******
**                                                    ******
** method  @get                                       ******
************************************************************
***********************************************************/
 

 
 const [loading, setLoading] = useState(true);
//const [errorMessage, setErrorMessage] = useState(null);  
 const [data, setData] = useState({"tasks": []}); 

 let planId=props.planId;
 let dayId=props.dayId;
 let url=`https://apis.figruop.com/myapissn/public/api/tasks/${planId}/${dayId}`;   
 
    
  /* useEffect(() => {
    const fetchData = async () => {
      const result = await axios({
          method: 'get',
          url: `https://apis.figruop.com/myapissn/public/api/tasks/${props.planId}/${props.dayId}`,
          headers: {'X-Requested-With': 'XMLHttpRequest'},
          responseType: 'json',
          status: 200,
        }

      )
      .then(setData)
        
      //setData(result.data);
        
        
        
    };
   setLoading(false);
  
       
       
  fetchData();
  }, [data]);   
  */
    
  /*   useEffect(() => {
    axios
      .get()
      .then(result => setData(result.data));
  }, [props.planId]); 
 */
  
  useEffect(() => {
    const fetchData = async () => {
   //   setIsLoading(true);
      const result = await axios(url);
      setData(result.data);
     // setIsLoading(false);
    };
      
      
    return(fetchData()
          
          );
      
  }, [planId]);  
    
    
    
    
    
    
    
    
  
    return (
 

        
<div className="column is-half">
<div className="class cal-shadow">

        
        
    <div className="col__head__content">
        <span className="col__head__number"> {new Intl.NumberFormat('ar-EG').format(props.dd)  }</span>  
        <p className="col__head__name"> {Month[props.mm]} </p>
        <p className="col__head__name">, {Days[props.name]}
        </p>
    </div>

        
        
     {data.tasks.map(item => (
           <Task 
            id={item.id}
            task={item.task} 
            level={item.level}
            />
     ))} 
        
                
     <div className="add__task">   
       <Create   
       planId={props.planId} 
       dayId={props.dayId}  
       />
     </div>

        


</div> 

</div> 



        
        

    );
  }
  



export default CalCol;