import React,{useState} from 'react';
import axios from 'axios';

import SideGoalLlist from './goals';




import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import ListItemText from '@material-ui/core/ListItemText';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';
import { withStyles } from '@material-ui/core/styles';




import TextField from '@material-ui/core/TextField';


import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';



const Green = withStyles({
  root: {
    color: '#8BC34A',
    '&$checked': {
      color: '#8BC34A',
    },
  },
  checked: {},
})(props => <Radio color="default" {...props} />
  );
   
 const Yellow = withStyles({
  root: {
    color: '#FFC107',
    '&$checked': {
      color: '#FFC107',
    },
  },
  checked: {},
})(props => <Radio color="default" {...props} />
  );

const Red = withStyles({
  root: {
    color: '#FF5722',
    '&$checked': {
      color: '#FF5722',
    },
  },
  checked: {},
})(props => <Radio color="default" {...props} />
  );

   
   




const useStyles = makeStyles(theme => ({
    
  root: {
    '& > *': {
      margin: 40,
      width:  '40%',
    },
  },
    appBar: {
    position: 'relative',
    margin: 0,
    width: '100%',   
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1,
    color: 'white',
  },
  button: {
    color: 'white',
  },
}));


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
















const initState = { 
        task: '',
        content: ' ',
        goalId: 5,      
     }





const reducer = (state, action) => {
  switch (action) {
    case 'OPEN':
      return true
    case 'CLOSE':
      return false
    default:
      throw new Error()
  }
}



function Create(props) {

  // dialog togled using reducer  
  const [state, dispatch] = React.useReducer(reducer, false);
  const handleClickOpen = () => dispatch('OPEN');
  const handleClose = () => dispatch('CLOSE');
    
  // declare plan id , day id from props
  let planId =  props.planId;
  let dayId =  props.dayId; 
    
  // using MATRIAL_UI useStyles above component 
  const classes = useStyles();
    
    
    
/***********************************************************
************************************************************
** prams @palnID , @dayId                             ******
**                                                    ******
** method  @post                                       ******
************************************************************
***********************************************************/


    
const [value ,setValue]=useState(2);   
 
const [taskData ,SetTaskData]=useState(initState);      
    
function handleChange(event){
 SetTaskData({...taskData,[event.target.name]: event.target.value});
 setValue(event.target.value);
};

    
function handleSubmit(e){
    e.preventDefault();
    axios.post('https://apis.figruop.com/myapissn/public/api/tasks',{ 
        pid: planId,
        did: dayId,
        task: taskData.task,
        content: taskData.content,
        level: value,
        gid: taskData.goalId,
    })
    .then((res) => {
          console.log(res) 
     })
       .catch(error => {
          console.log(error) 
     });
     SetTaskData({ 
        task: ' ',
        content: ' ',
     //   goalId: ' ',      
     });
     setValue(1);
     dispatch('CLOSE') 
    };
       
    

    
  return (
    <React.Fragment>
      
      
     <Button   onClick={handleClickOpen}
        style={{color:' rgb(134, 134, 134)',
    width: '100%',
    fontSize: '18px',
    fontWeight: '400', }}
        startIcon={<i className="fal fa-calendar-plus add"></i>}
      >
        اضافة تاسك جديد
      </Button>       
      
      
      
      <Dialog fullScreen open={state} onClose={handleClose} TransitionComponent={Transition}>
      
      
     <form className={classes.root} noValidate autoComplete="off"  onSubmit={handleSubmit}>
 
        <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" className={classes.button} onClick={handleClose} aria-label="close">
              <CloseIcon />
            </IconButton>
      
      
      
      
            <Typography variant="h6" className={classes.title}>
      
      
            <div>
      
      ١ رجب , ١٤١٦   
      
 -   الاسبوع الاول   
      
              </div>  
              
      
      
            </Typography>
      
      
      
      
            <Button autoFocus className={classes.button} type="submit">
              انشاء
            </Button>
          </Toolbar>
        </AppBar>
  
      
      
      
      
      
      
      
      
    <TextField id="standard-basic" label="عنوان التاسك" name="task" value={taskData.task} onChange={handleChange}  />
     
     
    <textarea name="content"  value={taskData.content}  onChange={handleChange}  />
      
   
    <FormControl component="fieldset">

    <FormLabel component="legend"> مستوي التاسك </FormLabel>

        <RadioGroup aria-label="position" name="position" value={value} onChange={handleChange} row>

            <FormControlLabel
              value="0"
              control={<Green color="green" />}
              label="سهل"
              labelPlacement="end"
            />

            <FormControlLabel
              value="1"
              control={<Yellow color="primary" />}
              label="متوسط"
              labelPlacement="end"
            /> 


            <FormControlLabel
              value="2"
              control={<Red color="primary" />}
              label="صعب"
              labelPlacement="end"
            />      

        </RadioGroup>
    </FormControl>
      
      

      
      
      
      < SideGoalLlist 
        id='planId'
      />
      
      
  
      
    </form>
      
      
   
      </Dialog>
    </React.Fragment>
  );
}





export default Create;