import React from 'react';
import axios from 'axios';




import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Switch from '@material-ui/core/Switch';

const useStyles = makeStyles(theme => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
    width: 'fit-content',
  },
  formControl: {
    marginTop: theme.spacing(2),
    minWidth: 120,
  },
  formControlLabel: {
    marginTop: theme.spacing(1),
  },
}));










export default function Delete(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [fullWidth, setFullWidth] = React.useState(true);
  const [maxWidth, setMaxWidth] = React.useState('sm');

  
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };   
    
    
    
    
/***********************************************************
************************************************************
** prams @taskId                                      ******
**                                                    ******
** method  @delet                                     ******
************************************************************
***********************************************************/    
    
 function handleSubmit(){
 //       e.preventDefault();
 
    axios.delete(`https://apis.figruop.com/myapissn/public/api/tasks/${props.id}`)
    .then((res) => {
          console.log(res) 
     })
       .catch(error => {
          console.log(error) 
     });

    setOpen(false);
               
    };


    



  return (
    <React.Fragment>

      
             
      <Button  onClick={handleClickOpen}
        style={{color: 'rgb(106, 113, 106)',width: '50%'}}
        startIcon={<i className="fal fa-calendar-times tdelete"></i>}
      >
        حذف
      </Button>  
      
      
      
      
      
      <Dialog
        fullWidth={fullWidth}
        maxWidth={maxWidth}
        open={open}
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
      >
      
      
      
      
      
      
        <DialogContent>
      
      
      
      <DialogTitle id="max-width-dialog-title">
      
      <h5>
      <i className="fal fa-calendar-times"></i>
      
      هل تريد حذف​
      
      </h5>
      </DialogTitle>

      
      
      
      <DialogContentText>
      
      هنا محتوى التاسك المراد حذفة هنا محتوى التاسك المراد حذفة
      
        
      
      
      </DialogContentText>
      
      
  
      
      
      
        </DialogContent>
        <DialogActions>
        <Button onClick={handleClose}>
            تراجع
          </Button>
      
      
          <Button onClick={handleSubmit} color="primary">
            تاكيد
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  );
}