import React, {/*Fragment*/ useState, useEffect } from 'react';
import axios from 'axios';
//import GoalLi from './helper/goalli';

function GoalsList(props) {

    
let planId = props.planId  
       /*  {data.goals.map(item => (<GoalLi id={item.id} plan_id={item.plan_id}   goal={item.goals} />))} */
  
const [loading, setLoading] = useState(true);
//const [errorMessage, setErrorMessage] = useState(null);  
    
  const [data, setData] = useState({ goals: [] });
    
  useEffect(() => {
    const fetchData = async () => {
      const result = await axios({
          method: 'get',
          url: `https://apis.figruop.com/myapissn/public/api/goals/pid=${planId}`,
          headers: {'X-Requested-With': 'XMLHttpRequest'},
          responseType: 'json',
          status: 200,
        }

      );
      setData(result.data);
        
      setLoading(false);
        
        
    };
      
    fetchData();
  }, [data]);


  
    return (
 
    <div>
        
     {loading ? (
         <span>loading...</span>

        ) : (
      
           <ul>
    
      
      
           </ul>

        
        )}    
    

    </div>

    );
  }
  



export default GoalsList;