import React, {Fragment, useState } from 'react';
import axios from 'axios';
//import { useHistory} from "react-router-dom";


const Modal = (props) => {
    
    
const [newPlan, setNewPlan] = useState({
  Title: ' ',
  Description:' ',
  Excerpt: ' ',
});  
    
const [isOpen, setIsOpen] = React.useState(false);
const [value, setValue] = React.useState(1);
    
    
let axiosConfig = {
  headers: {
      'Content-Type': 'application/json;charset=UTF-8',
      "Access-Control-Allow-Origin": "*",
      'X-Requested-With': 'XMLHttpRequest',
      'Access-Control-Allow-Headers': '*',
      'Access-Control-Allow-Methods':'*',

  }
};
    
    
    
const handleChange = (event) => {
       setNewPlan({...newPlan, [event.target.name]: event.target.value})
}

const handleSubmit = (e) => {
      e.preventDefault();
      axios.post('https://fakerestapi.azurewebsites.net/api/Books', newPlan,axiosConfig)
      .then(function (response) {
           console.log(response)
       })
       .catch(function (error) {
           console.log(error)
       }) 
        

    }
 
    
    
    
    
    
    
  return (
      <Fragment>
      
      
   
      
      
  <a className="navbar-item" onClick={()=>setIsOpen(true)}>
      <i className="fas fa-plus-square hicon"></i>
  </a>
   
      
 
     {isOpen && (
         
         
         
<div style={{height: '100vh',position: 'absolute',top: '0',background: 'rgba(0, 0, 0, 0.52)',width: '100%', zIndex: '999',}} >
        
      
        
   <div style={{ height: 'auto',width: '500px',background: 'white',zIndex: '999',margin: '17vh auto 0px',borderRadius: '10px',}}>
   <span onClick={()=>setIsOpen(false)} style={{display: 'inline-block',
    padding: '5px 15px',
    float: 'left',
    margin: '10px',
    borderRadius: '50%',
    background: 'rgb(244, 245, 247)',
    fontSize: '25px', }}>
    <i className="fal fa-times"></i>
    </span>
      
      
      
      
      
       <form  onSubmit={handleSubmit}>    
  
        
        <h2 style={{textAlign: 'right',fontSize: '25px',padding: '15px 40px',fontWeight: 'bold',color: '#787979',}}> انشاء خطة جديدة </h2>
        

  
  
  
  




         
        
        <input type="text" style={{fontSize: '20px',padding: '15px 10px',margin: '7.5%',width: '85%',border: 'none',borderBottom: '2px solid  #8BC34A',background: '#f4f5f7',}} name="Title" value={newPlan.Title}  placeholder="عنوان الخطة" />
        
            
        
        <input type="text" style={{fontSize: '20px',padding: '15px 10px',margin: '7.5%',width: '85%',border: 'none',borderBottom: '2px solid  #8BC34A',background: '#f4f5f7',}} name="Description" value={newPlan.Description}  placeholder="عنوان الخطة" />        

        


        <input type="text" style={{fontSize: '20px',padding: '15px 10px',margin: '7.5%',width: '85%',border: 'none',borderBottom: '2px solid  #8BC34A',background: '#f4f5f7',}}  name="Excerpt" value={newPlan.Excerpt} />
            
            



<div style={{display: 'block',}}>
    
        <button onClick={()=>setIsOpen(false)} style={{margin: '40px', padding: '12px 20px',fontSize: '20px',color: 'white',border: 'none',background: '#787979',}} >
         <i className="fal fa-times"></i>   اغلاق
        </button>

        <button  style={{margin: '40px', padding: '12px 20px',fontSize: '20px',color: 'white',border: 'none',background: 'rgb(139, 195, 74)',float: 'left',}} >
          <i className="fal fa-calendar-edit" type="submit"></i>   انشاء
        </button>

</div>










    </form>    








        </div>

        
    </div>


        
      )}
      </Fragment>

      
  );
};




export default Modal;

