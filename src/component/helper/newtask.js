import React, {Fragment, useState } from 'react';


function NewTask( ) {

   const [newTask, setNewTask] = React.useState(false);
     

  
    return (
      <Fragment>
        
    <button className="add__task"  onClick={()=>setNewTask(true)}>
     <ul>
        <li>
        <i className="fal fa-plus-circle"></i> 
        </li>
        <li>
        <span className="add__task__name"  style={{padding: '5px 0px',display: 'grid',}}>اضف مهمة جديدة  
        </span> 
        </li>
     </ul>
    </button>
        
  
      
     {newTask && (

     <div style={{height: '100vh',position: 'absolute',top: '0',background: 'rgba(0, 0, 0, 0.52)',width: '100%', zIndex: '999',left: 0,}} >
       
      
        
      
        
        <div style={{ height: '100%',width: '100%',background: 'white',zIndex: '999',}}>
      
        <span onClick={()=>setNewTask(false)} style={{display: 'inline-block',
    padding: '5px 15px',
    float: 'left',
    margin: '10px',
    borderRadius: '50%',
    background: 'rgb(244, 245, 247)',
    fontSize: '25px', }}>
             <i className="fal fa-times"></i>
        </span>
      
      
      
      
      
        
        
        <h2 style={{textAlign: 'right',fontSize: '25px',padding: '15px 40px',fontWeight: 'bold',color: '#787979',}}> انشاء خطة جديدة </h2>
        






        
        
        <input type="text" style={{fontSize: '20px',padding: '15px 10px',margin: '7.5%',width: '85%',border: 'none',borderBottom: '2px solid  #8BC34A',background: '#f4f5f7',}} placeholder="عنوان الخطة" />
        
        
        
        
        <h6 style={{fontSize: '20px', color: '#787979', padding: '10px 40px',}}> 
        مدة الخطة
        </h6>
        
        
        








<div style={{display: 'block',}}>
    
        <button onClick={()=>setNewTask(false)} style={{margin: '40px', padding: '12px 20px',fontSize: '20px',color: 'white',border: 'none',background: '#787979',}} >
         <i className="fal fa-times">
             </i>   اغلاق
        </button>

        <button onClick={()=>setNewTask(false)} style={{margin: '40px', padding: '12px 20px',fontSize: '20px',color: 'white',border: 'none',background: 'rgb(139, 195, 74)',float: 'left',}} >
          <i className="fal fa-calendar-edit">
              </i>   انشاء
        </button>

</div>


















        </div>

        
    </div>
        
      )}



      </Fragment>
    );
  }
  



export default NewTask;