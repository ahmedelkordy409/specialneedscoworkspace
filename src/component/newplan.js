import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import Switch from '@material-ui/core/Switch';


import Tooltip from '@material-ui/core/Tooltip';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';



import TextField from '@material-ui/core/TextField';

import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';









const useStyles = makeStyles(theme => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
    width: 'fit-content',
  },
  formControl: {
    marginTop: theme.spacing(2),
    minWidth: 120,
  },
  formControlLabel: {
    marginTop: theme.spacing(1),
  },
}));


const style = {
  background: 'rgb(236, 145, 121)',
  borderRadius: 0,
  border: 0,
  color: 'white',
  height: 48,
  padding: '0 30px',
  boxShadow: 'rgba(255, 255, 255, 0.3) 0px 3px 5px 2px',
};



export default function MaxWidthDialog() {
  const classes = useStyles();
  const [selectedDate, setSelectedDate] = React.useState(new Date('2014-08-18T21:11:54'));

  const handleDateChange = date => {
    setSelectedDate(date);
  };
    
      
    
    
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };



  return (
    <React.Fragment>
      
      
        
          
      
      
<Tooltip disableFocusListener title={<h2>  انشاء خطة جديدة​ </h2>} style={{fontSize:'30px',}}>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
      <i className="fas  fa-chalkboard-teacher  hicon"> </i>
      </Button>
</Tooltip>   

      
      
      
      
      
      
      <Dialog
        maxWidth='330px'
        open={open}
        onClose={handleClose}
        aria-labelledby="max-width-dialog-title"
      >
      
      
<DialogTitle id="max-width-dialog-title">            انشاء خطة جديدة
</DialogTitle>
      
      
      <DialogContent>
      
          <form className={classes.form} noValidate>

      
      
      
             <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Grid container justify="space-around">

        <KeyboardDatePicker
          margin="normal"
          id="date-picker-dialog"
          label="تاريخ البداء"
          format="MM/dd/yyyy"
          value={selectedDate}
          onChange={handleDateChange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
      </Grid>
        

   
        
    </MuiPickersUtilsProvider> 
        
        
      
      
      
      
      
      
          </form>
      
        </DialogContent>
      

        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Close
          </Button>
        </DialogActions>
      
      
      
      
      </Dialog>
      
      
      
      
    </React.Fragment>
  );
}